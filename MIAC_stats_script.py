''' MIAC_stats_script.py

    A script for converting the city data in "Baltimore,14,26,47..." form into
    rows that match the populations table structure "Baltimore,1790,14",
    "Baltimore,1800,26", etc.
'''

import sys
import csv

reader = csv.reader(sys.stdin)
writer = csv.writer(sys.stdout)

headingRow = reader.next()[1:]

players = {}
for row in reader:
    row = map(str.strip, row)
    assert len(row) == len(headingRow) + 1
    players[row[0]] = row[1:]

for player in players:
    a = []
    for k in range(len(headingRow)):
        playerRow = players[player]
        statString = headingRow[k]
        cell = playerRow[k]
        if cell == '-':
            cell = '-1'
        a.append(cell)
    writer.writerow([player, a[ 0 ], 
a[ 1 ], 
a[ 2 ], 
a[ 3 ], 
a[ 4 ], 
a[ 5 ], 
a[ 6 ], 
a[ 7 ], 
a[ 8 ], 
a[ 9 ], 
a[ 10 ], 
a[ 11 ], 
a[ 12 ], 
a[ 13 ], 
a[ 14 ], 
a[ 15 ], 
a[ 16 ], 
a[ 17 ], 
a[ 18 ], 
a[ 19 ], 
a[ 20 ], 
a[ 21 ], 
a[ 22 ], 
a[ 23 ], 
a[ 24 ], 
a[ 25 ], 
a[ 26 ], 
a[ 27 ], 
a[ 28 ],
a[ 29 ],
a[ 30 ],
a[ 31 ],
a[ 32 ],
a[ 33 ],
a[ 34 ],
a[ 35 ],
a[ 36 ],
a[ 37 ],
a[ 38 ],
a[ 39 ]])
