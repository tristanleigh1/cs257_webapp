#!/usr/bin/env python
import unittest
from controller import *

class ControllerTest(unittest.TestCase):
	"""Test class for controller.py"""

	def setUp(self):
		""" Setting up for the test """
		pass

	def test_sanitize_user_input_should_remove_selected_characters(self):
		expected = ""
		actual = sanitizeUserInput(";,\\/:\"<>@()")
		self.assertEqual(actual, expected)

if __name__ == '__main__':
    unittest.main()