#!/usr/bin/env python
import unittest
from model import *
from data_source import DataSource


class ModelTest(unittest.TestCase):
	"""Test class for model.py"""

	def setUp(self):
		""" Setting up for the test """
		pass

	def test_get_page_template_should_return_correct_template(self):
		expected = """\t\t</div>\n\t</body>\n\n\t<footer></footer>\n</html>"""
		actual = getPageTemplate("ending_template.html")
		self.assertEqual(actual, expected)

	def test_construct_page_as_html_should_return_correct_html_string(self):
		expected = """<!DOCTYPE html>
<html lang="en">
	<head>
		<meta-charset="utf-8">
		<title>PER-pal</title>
		<link rel=stylesheet href="perstyle.css" type="text/css">
	</head>
	<body>
		<div class="top">
		    <a id="perlink" href="index.py">PER-Pal</a>
		    <div class="topbox">
				<em>Explore player efficiency ratings (PERs) for men's basketball players in the Minnesota Intercollegiate Athletic Conference (MIAC)</em>
		    </div>
		</div>
		<div class="left">
		    <a class="sidelink" id="ind_link" href="index.py?stats_type=ind&team_name=&player_name=&letter_index=">Individual Statistics</a>
		    <a class="sidelink" href="index.py?stats_type=team">Team Statistics</a>
		    <a class="sidelink" href="index.py?stats_type=league_leaders&sorting_num=">League Leaders</a>
		    <a class="sidelink" href="index.py?stats_type=about">About Statistics</a>
		</div>
		<div class="main">

<h1>Corey Abbas<br/>Concordia 2014-2015 Statistics<br/></h1><table border="1" style="width:100%"><tr><td><strong>No.</strong></td><td><strong>Player</strong></td><td><strong>GP</strong></td><td><strong>GS</strong></td><td><strong>MIN/G</strong></td><td><strong>FG-FGA</strong></td><td><strong>PCT</strong></td><td><strong>3FG-3FGA</strong></td><td><strong>PCT</strong></td><td><strong>FT-FTA</strong></td><td><strong>PCT</strong></td><td><strong>ORB/G</strong></td><td><strong>DRB/G</strong></td><td><strong>REB/G</strong></td><td><strong>A/G</strong></td><td><strong>TO/G</strong></td><td><strong>A/TO</strong></td><td><strong>BLK/G</strong></td><td><strong>STL/G</strong></td><td><strong>PPG</strong></td><td><strong>PER</strong></td></tr><tr><td>14</td><td>Corey Abbas</td><td>8</td><td>-</td><td>4.4</td><td>4-15</td><td>26.7</td><td>2-4</td><td>50.0</td><td>0-0</td><td>-</td><td>0.0</td><td>0.38</td><td>0.4</td><td>0.3</td><td>0.1</td><td>2.0</td><td>0.0</td><td>0.3</td><td>1.3</td><td>7.3</td></tr></table>		</div>
	</body>

	<footer></footer>
</html>"""
		actual = constructPageAsHtml({'stats_type':'ind', 'team_name':'Concordia', 'player_name':'Corey Abbas', 'sorting_num':'', 'letter_index':''})
		self.assertEqual(actual, expected)

	def test_get_main_content_should_return_correct_html_string(self):
		expected = """<h1>Corey Abbas<br/>Concordia 2014-2015 Statistics<br/></h1><table border="1" style="width:100%"><tr><td><strong>No.</strong></td><td><strong>Player</strong></td><td><strong>GP</strong></td><td><strong>GS</strong></td><td><strong>MIN/G</strong></td><td><strong>FG-FGA</strong></td><td><strong>PCT</strong></td><td><strong>3FG-3FGA</strong></td><td><strong>PCT</strong></td><td><strong>FT-FTA</strong></td><td><strong>PCT</strong></td><td><strong>ORB/G</strong></td><td><strong>DRB/G</strong></td><td><strong>REB/G</strong></td><td><strong>A/G</strong></td><td><strong>TO/G</strong></td><td><strong>A/TO</strong></td><td><strong>BLK/G</strong></td><td><strong>STL/G</strong></td><td><strong>PPG</strong></td><td><strong>PER</strong></td></tr><tr><td>14</td><td>Corey Abbas</td><td>8</td><td>-</td><td>4.4</td><td>4-15</td><td>26.7</td><td>2-4</td><td>50.0</td><td>0-0</td><td>-</td><td>0.0</td><td>0.38</td><td>0.4</td><td>0.3</td><td>0.1</td><td>2.0</td><td>0.0</td><td>0.3</td><td>1.3</td><td>7.3</td></tr></table>"""
		actual = getMainContent({'stats_type':'ind', 'team_name':'Concordia', 'player_name':'Corey Abbas', 'sorting_num':'', 'letter_index':''})
		self.assertEqual(actual, expected)

if __name__ == '__main__':
    unittest.main()