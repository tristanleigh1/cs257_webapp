#View.py
#Julia Kroll and Tristan Leigh

import string
from data_source import DataSource

ds = DataSource()

def makeHomePage():
    """ Returns an HTML string that creates the Home Page for our webapp."""
    home_page = ""
    home_page += "<h2>Welcome to PER pal!</h2>"
    home_page += "<p>The Player Efficiency Rating (PER) is a per-minute rating developed by ESPN.com columnist John Hollinger. In John's words, \"The PER sums up all a player's positive accomplishments, subtracts the negative accomplishments, and returns a per-minute rating of a player's performance.\"</p>"
    home_page += "<h3>Top 10 League Leaders in PER</h3>"
    lg_leaders = ds.getLeagueLeaders(10)
    home_page += makeTable(lg_leaders, 'league_leaders')
    return home_page


def makeTitle(school, player):
    """ Returns an HTML string that creates a title for the statistics table."""
    titleHTML = "<h1>"
    if player != "": # player title
        titleHTML += "%s<br/>" % player
    if school != "": # school title
        titleHTML += "%s 2014-2015 Statistics<br/>" % school
    else: # league leaders title
        titleHTML += "Top %d PERs<br/>" % sorting_num
    titleHTML += "</h1>"
    return titleHTML

def makeTable(player_names_arr, stats_type):
    """ Returns an HTML string that creates a table of statistics for the given player(s)."""
    # Generate header row for table
    table_headings = ['No.', 'Player', 'GP', 'GS', 'MIN/G', 'FG-FGA', 'PCT', '3FG-3FGA', 'PCT', 'FT-FTA', 'PCT', 'ORB/G', 'DRB/G', 'REB/G', 'A/G', 'TO/G', 'A/TO', 'BLK/G', 'STL/G', 'PPG', 'PER']
    if stats_type == 'league_leaders':
        table_headings.insert(0, 'Rank')
    table = "<table border=\"1\" style=\"width:100%\"><tr>"
    for i in table_headings:
        table += "<td><strong>" + i + "</strong></td>"
    table += "</tr>"
        
    # Generate one row per player
    rank_count = 1
    for name in player_names_arr:
        player_row = "<tr>"
        player_data = ds.getPlayerDataForTable(name)
        # Construct each cell 
        if stats_type == 'league_leaders':
            cell_string = "<td>" + str(rank_count) + "</td>"
            rank_count += 1
            player_row += cell_string
        for num in player_data:
            if num == -1:  # No attempts by player
                num = '-'
            elif type(num) == float:
                num = round(num,2)
            else:
                pass
            cell_string = "<td>" + str(num) + "</td>"
            player_row += cell_string
        player_row += "</tr>"
        table += player_row

    # Complete table
    table += "</table>"
    return table

def makeList(list_type,list_content):
    """ Returns an HTML string that creates a list of players, teams, or league leaders."""
    list_string = ""
    # Team Statistics
    if list_type == 'teams_by_team':
        list_string += "<h2>Choose Team:</h2>"
        list_string += "<ul>"
        if list_content == "":
            team_list = ds.getTeamNames()
            for team in team_list:
                url = "index.py?stats_type=team&team_name=" + team
                list_string += "<li><a href=\"" + url + "\">" + team + "</a></li>"
        list_string += "</ul>"

    # League Leaders
    elif list_type == 'league_leaders':
        title = "<h2>Number of League Leaders:</h2>"
        leader_num_list = """<ul class="Sorting Numbers">
                                 <li><a href="index.py?stats_type=league_leaders&sorting_num=5">Top 5</a></li>
                                 <li><a href="index.py?stats_type=league_leaders&sorting_num=10">Top 10</a></li>
                                 <li><a href="index.py?stats_type=league_leaders&sorting_num=25">Top 25</a></li>
                                 <li><a href="index.py?stats_type=league_leaders&sorting_num=50">Top 50</a></li>
                                 <li><a href="index.py?stats_type=league_leaders&sorting_num=100">Top 100</a></li>
                                 <li><a href="index.py?stats_type=league_leaders&sorting_num=all">See All</a></li>
                            </ul>"""
        list_string = title + leader_num_list

    # Individual Statistics By Last Name
    elif list_type == 'player_and_team':
        list_string += "<h2>Player (Team):</h2>"
        list_string += "<ul>"
        letter = "a"
        if list_content != "":
            letter = list_content
        player_team_list = ds.getPlayersNamesAndTeamByLastName(letter)
        for item in player_team_list:
            player_info = "<li><a href='index.py?stats_type=ind&team_name=" + item[1] + "&player_name=" + item[0] + "'>" + item[0] + " (" + item[1] + ")</a></li>"
            list_string += player_info
        if len(player_team_list) == 0:
            list_string += "No players with last name " + letter + "."
        list_string += "</ul>"

    # Individual Statistics By Team
    elif list_type == 'inds_by_team':
        if list_content == "":
            list_string += "<h2>Choose Team:</h2>"
            list_string += "<ul>"
            team_list = ds.getTeamNames()
            for team in team_list:
                url = "index.py?stats_type=ind&team_name=" + team + "&player_name="
                list_string += "<li><a href=\"" + url + "\">" + team + "</a></li>"
            list_string += "</ul>"
        else:
            list_string += "<h2>Choose Player:</h2>"
            list_string += "<ul>"
            player_list = ds.getPlayersNames(list_content) # list_content = team name
            for player in player_list:
                url = "index.py?stats_type=ind&team_name=" + list_content + "&player_name=" + player
                list_string += "<li><a href=\"" + url + "\">" + player + "</a></li>"
            list_string += "</ul>"

    return list_string

def makeLetterIndices():
    """ Returns HTML string creating a link for each letter of the alphabet to sort players by last name."""
    alphabet = string.uppercase[:26]
    links_string = ""
    for letter in alphabet:
        url = "index.py?stats_type=ind&letter_index=" + letter
        link = "<a href=\"" + url + "\">" + letter + "</a>"
        if letter != 'Z':
            link += " | "
        links_string += link
    links_string += "<br/>"
    return links_string
