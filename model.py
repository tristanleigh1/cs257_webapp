#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""A web application that displays PER (player efficiency ratings) for men's basketball players in the MIAC.
By Tristan Leigh and Julia Kroll, 2015; adapted from Jadrian Miles sample, 2015."""
from view import *
from data_source import DataSource

ds = DataSource()

def constructPageAsHtml(parameters):
    """Prints to standard output the main page for this web application, based
    on the specified template file and parameters. Takes in six parameters:
    stats_type: "", ind, team, league_leaders
    team_name: "", all, (one of 11 teams in the MIAC)
    player_name: "", (one of 179 players in the MIAC)
    sorting_num: "", (integer determining how many league leaders to display), all
    letter_index: "", (a letter)
    """
    
    # actually call methods in constructPage:
    beginning = getPageTemplate("beginning_template.html")
    main = getMainContent(parameters)
    ending = getPageTemplate("ending_template.html")
    page = ""
    if main == None:
        page = "main is none"
    else: 
        page = beginning + main + ending
    return page

def getPageTemplate(template_filename):
    """ Opens a file template."""
    try:
        f = open(template_filename)
        template = f.read()
        f.close()
    except Exception, e:
        template = "Cannot read template file <tt>%s</tt>." % (
            template_filename)
    return template
    
def getMainContent(parameters):
    """ Returns an HTML string consisting of the main body in the page as determined by the five parameters."""
    content = ""
    stats_type = parameters['stats_type']
    team_name = parameters['team_name']
    player_name = parameters['player_name']
    letter_index = parameters['letter_index']
    sorting_num = parameters['sorting_num']
    if stats_type == 'ind':
        # this is creating links for "By Team" and "By Last Name"
        if team_name == "" and player_name == "" and letter_index == "":
            content += "<h2>Search For Player:</h2>"
            content += "<div id='bylinks'>"
            #url for "By Team" Link
            content += "<a href='index.py?stats_type=ind&team_name=all&player_name='>By Team</a><br/>"
            #url for "By Last Name" Link
            content += "<a href='index.py?stats_type=ind&team_name=&player_name=&letter_index=A'>By Last Name</a>"
            content += "</div>"
        # this is making list of team names after selecting "By Team"
        elif team_name == "all" and player_name == "":
            content += makeList('inds_by_team',"")
        # this is making a list of all individuals on a given team
        elif team_name != "" and player_name == "" and ds.isTeam(team_name):
            content += makeList('inds_by_team', team_name)
        # this is individual statistics by last name
        elif team_name == "" and player_name == "" and letter_index != "":
            content += makeLetterIndices()
            content += makeList('player_and_team',letter_index)
        # this is one player's statistics after selecting "By Last Name"
        elif team_name != "" and ds.isPlayer(player_name) and ds.isTeam(team_name):
            content += makeTitle(team_name, player_name)
            content += makeTable([player_name], stats_type)
        else:
            content += makeHomePage()

    elif stats_type == 'team':
        # this is creating a list of team names after selecting "Team Statistics"
        if team_name == "":
            content += makeList('teams_by_team',"")
        # this is creating a table of players statistics on one team
        elif team_name != "" and ds.isTeam(team_name):
            players = ds.getPlayersNames(team_name)
            if len(players) != 0:
                content += makeTitle(team_name, "")
                content += makeTable(players, stats_type)
            else:
                content += makeHomePage()
        else:
            content += makeHomePage()

    elif stats_type == 'league_leaders':
        # this is creating a list of numbers of league leaders
        if sorting_num == "":
            content += makeList('league_leaders',"")
        # this is creating the table of league leaders
        else:  # sorting_num != ""
            try:
                players = ds.getLeagueLeaders(sorting_num)
                if len(players) > 0:
                    content += makeTable(players, stats_type)
            except:
                print 'Sorting number is not an integer.'

    elif stats_type == 'about':
        content += getPageTemplate("about_template.html")
    # home page if invalid parameters or no parameters
    else:
        content += makeHomePage()
    return content
