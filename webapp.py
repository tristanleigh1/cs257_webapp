#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""A web application that displays PER (player efficiency ratings) for men's basketball players in the MIAC.
By Tristan Leigh and Julia Kroll, 2015; adapted from Jadrian Miles sample, 2015."""
import string
from data_source import DataSource

ds = DataSource()

def constructPageAsHtml(stats_type, team_name, player_name, sorting_num, letter_index):
    """Prints to standard output the main page for this web application, based
    on the specified template file and parameters. Takes in six parameters:
    stats_type: "", ind, team, league_leaders
    team_name: "", all, (one of 11 teams in the MIAC)
    player_name: "", (one of 179 players in the MIAC)
    sorting_num: "", (integer determining how many league leaders to display), all
    letter_index: "", (a letter)
    """
    
    def getPageTemplate(template_filename):
        """ Opens a file template."""
        try:
            f = open(template_filename)
            template = f.read()
            f.close()
        except Exception, e:
            template = "Cannot read template file <tt>%s</tt>." % (
                template_filename)
        return template
        
    def getMainContent():
        """ Returns an HTML string consisting of the main body in the page as determined by the five parameters."""
        content = ""
        if stats_type == 'ind':
            # this is creating links for "By Team" and "By Last Name"
            if team_name == "" and player_name == "" and letter_index == "":
                #url for "By Team" Link
                content += "<a href = 'index.py?stats_type=ind&team_name=all&player_name='>By Team</a><br/>"
                #url for "By Last Name" Link
                content += "<a href = 'index.py?stats_type=ind&team_name=&player_name=&letter_index=A'>By Last Name</a>"
            # this is making list of team names after selecting "By Team"
            elif team_name == "all" and player_name == "":
                content += makeList('inds_by_team',"")
            # this is making a list of all individuals on a given team
            elif team_name != "" and player_name == "":
                content += makeList('inds_by_team', team_name)
            # this is individual statistics by last name
            elif team_name == "" and player_name == "" and letter_index != "":
                content += makeLetterIndices()
                content += makeList('player_and_team',letter_index)
            # this is one player's statistics after selecting "By Last Name"
            elif team_name != "":
                content += makeTitle(team_name, player_name)
                content += makeTable([player_name], stats_type)

        elif stats_type == 'team':
            # this is creating a list of team names after selecting "Team Statistics"
            if team_name == "":
                content += makeList('teams_by_team',"")
            # this is creating a table of players statistics on one team
            elif team_name != "":
                players = ds.getPlayersNames(team_name)
                content += makeTitle(team_name, "")
                content += makeTable(players, stats_type)

        elif stats_type == 'league_leaders':
            # this is creating a list of numbers of league leaders
            if sorting_num == "":
                content += makeList('league_leaders',"")
            # this is creating the table of league leaders
            elif sorting_num != "":
                try:
                    players = ds.getLeagueLeaders(sorting_num)
                    content += makeTable(players, stats_type)
                except:
                    print 'Sorting number is not an integer.'
        # home page if invalid parameters or no parameters
        else:
            content += "<h2>Welcome to PER pal!</h2>"
            content += "<p>The Player Efficiency Rating (PER) is a per-minute rating developed by ESPN.com columnist John Hollinger. In John's words, \"The PER sums up all a player's positive accomplishments, subtracts the negative accomplishments, and returns a per-minute rating of a player's performance.\"</p>"
            content += "<h3>Top 10 League Leaders in PER</h3>"
            lg_leaders = ds.getLeagueLeaders(10)
            content += makeTable(lg_leaders, stats_type)
        return content
    
    # actually call methods in constructPage:
    beginning = getPageTemplate("beginning_template.html")
    main = getMainContent()
    ending = getPageTemplate("ending_template.html")
    page = ""
    if main == None:
        page = "main is none"
    else: 
        page = beginning + main + ending
    return page

def makeTitle(school, player):
    """ Returns an HTML string that creates a title for the statistics table."""
    titleHTML = "<h1>"
    if player != "": # player title
        titleHTML += "%s<br/>" % player
    if school != "": # school title
        titleHTML += "%s 2014-2015 Statistics<br/>" % school
    else: # league leaders title
        titleHTML += "Top %d PERs<br/>" % sorting_num
    titleHTML += "</h1>"
    return titleHTML

def makeTable(player_names_arr, stats_type):
    """ Returns an HTML string that creates a table of statistics for the given player(s)."""
    # Generate header row for table
    table_headings = ['No.', 'Player', 'GP', 'GS', 'MIN/G', 'FG-FGA', 'PCT', '3FG-3FGA', 'PCT', 'FT-FTA', 'PCT', 'ORB/G', 'DRB/G', 'REB/G', 'A/G', 'TO/G', 'A/TO', 'BLK/G', 'STL/G', 'PPG', 'PER']
    if stats_type == 'league_leaders':
        table_headings.insert(0, 'Rank')
    table = "<table border=\"1\" style=\"width:100%\"><tr>"
    for i in table_headings:
        table += "<td><strong>" + i + "</strong></td>"
    table += "</tr>"
        
    # Generate one row per player
    rank_count = 1
    for name in player_names_arr:
        player_row = "<tr>"
        player_data = ds.getPlayerDataForTable(name)
        # Construct each cell 
        if stats_type == 'league_leaders':
            cell_string = "<td>" + str(rank_count) + "</td>"
            rank_count += 1
            player_row += cell_string
        for num in player_data:
            if num == -1:
                num = '-'
            cell_string = "<td>" + str(num) + "</td>"
            player_row += cell_string
        player_row += "</tr>"
        table += player_row

    # Complete table
    table += "</table>"
    return table

def makeList(list_type,list_content):
    """ Returns an HTML string that creates a list of players, teams, or league leaders."""
    list_string = ""
    # Team Statistics
    if list_type == 'teams_by_team':
        list_string += "<h2>Choose Team:</h2>"
        list_string += "<ul>"
        if list_content == "":
            team_list = ds.getTeamNames()
            for team in team_list:
                url = "index.py?stats_type=team&team_name=" + team
                list_string += "<li><a href=\"" + url + "\">" + team + "</a></li>"
        list_string += "</ul>"

    # League Leaders
    elif list_type == 'league_leaders':
        title = "<h2>Number of League Leaders:</h2>"
        leader_num_list = """<ul class="Sorting Numbers">
                                 <li><a href="index.py?stats_type=league_leaders&sorting_num=5">Top 5</a></li>
                                 <li><a href="index.py?stats_type=league_leaders&sorting_num=10">Top 10</a></li>
                                 <li><a href="index.py?stats_type=league_leaders&sorting_num=25">Top 25</a></li>
                                 <li><a href="index.py?stats_type=league_leaders&sorting_num=50">Top 50</a></li>
                                 <li><a href="index.py?stats_type=league_leaders&sorting_num=100">Top 100</a></li>
                                 <li><a href="index.py?stats_type=league_leaders&sorting_num=all">See All</a></li>
                            </ul>"""
        list_string = title + leader_num_list

    # Individual Statistics By Last Name
    elif list_type == 'player_and_team':
        list_string += "<h2>Player (Team):</h2>"
        list_string += "<ul>"
        letter = "a"
        if list_content != "":
            letter = list_content
        player_team_list = ds.getPlayersNamesAndTeamByLastName(letter)
        for item in player_team_list:
            player_info = "<li><a href='index.py?stats_type=ind&team_name=" + item[1] + "&player_name=" + item[0] + "'>" + item[0] + " (" + item[1] + ")</a></li>"
            list_string += player_info
        if len(player_team_list) == 0:
            list_string += "No players with last name " + letter + "."
        list_string += "</ul>"

    # Individual Statistics By Team
    elif list_type == 'inds_by_team':
        if list_content == "":
            list_string += "<h2>Choose Team:</h2>"
            list_string += "<ul>"
            team_list = ds.getTeamNames()
            for team in team_list:
                url = "index.py?stats_type=ind&team_name=" + team + "&player_name="
                list_string += "<li><a href=\"" + url + "\">" + team + "</a></li>"
            list_string += "</ul>"
        else:
            list_string += "<h2>Choose Player:</h2>"
            list_string += "<ul>"
            player_list = ds.getPlayersNames(list_content) # list_content = team name
            for player in player_list:
                url = "index.py?stats_type=ind&team_name=" + list_content + "&player_name=" + player
                list_string += "<li><a href=\"" + url + "\">" + player + "</a></li>"
            list_string += "</ul>"

    return list_string

def makeLetterIndices():
    """ Returns HTML string creating a link for each letter of the alphabet to sort players by last name."""
    alphabet = string.uppercase[:26]
    links_string = ""
    for letter in alphabet:
        url = "index.py?stats_type=ind&letter_index=" + letter
        link = "<a href=\"" + url + "\">" + letter + "</a>"
        if letter != 'Z':
            link += " | "
        links_string += link
    links_string += "<br/>"
    return links_string
