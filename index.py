#!/usr/bin/env python
# -*- coding: utf-8 -*-
#index.py
from controller import *
from model import *
import cgi

def main():
    parameters = getCgiParameters()
    page = constructPageAsHtml(parameters)
    print 'Content-type: text/html\r\n\r\n',
    print page

if __name__ == '__main__':
    main()