#!/usr/bin/python
# Database Interface for cs257_webapp
# Julia Kroll and Tristan Leigh

# import cgitb
# cgitb.enable()

import os.path 
import sys
import psycopg2

class DataSource:
    
    def __init__(self):
        """Constructor for the DataSource database interface class.
        Takes in a data file containing a list of players and their respective information, 
        as well as a list of all teams in the conference, and sets up the database 
        accordingly."""
        USERNAME = 'leight'
        DB_NAME = 'leight'
        #print 'Reading your password...'
        try:
            f = open(os.path.join('/cs257', USERNAME))
            PASSWORD = f.read().strip()
            f.close()
            #print 'Success!<br/>'
            #print 'Your database password is %s.<br/>' % PASSWORD
        except:
            #print 'Failed. =(<br/>'
            sys.exit()
        #print 'Connecting to database %s...' %'ind_stats'
        try:
            conn = psycopg2.connect(user=USERNAME,
                                        database=DB_NAME,
                                        password=PASSWORD)
            #print 'Success!<br/>'
        except:
            #print 'Failed. =(<br/>'
            sys.exit()
        #print 'Making cursor...'
        try:
            self.cur = conn.cursor()
            #print 'Success!<br/>'
        except:
            #print 'Failed. =(<br/>'
            sys.exit()
 
    def getPlayerDataForTable(self, player_name):
        """Returns a list of 19 statistics, in addition to the jersey number and name, for one 
        player. This data will be displayed in a table. """
        try:
            self.cur.execute("SELECT num,full_name,gp,gs,min_avg,fg_fga,fg_pct,threefg_threefga,threefg_pct,ft_fta,ft_pct,off_reb_g,def_reb_g,reb_avg,assist_g,to_g,a_to,blk_g,stl_g,points_g,per FROM ind_stats WHERE full_name=%s;", (player_name, ))
            row_data = []
            for row in self.cur:
                for i in range(len(row)):
                    row_data.append(row[i])
        except:
            print 'Problem with getPlayerDataForTable() select query. =('
        return row_data

    def isPlayer(self, player_name):
        """Returns a boolean determining whether a player is in the database."""
        player_exists = False
        self.cur.execute("SELECT full_name FROM ind_stats WHERE full_name=%s;", (player_name, ))
        if self.cur.fetchone():
            player_exists = True
        return player_exists

    def isTeam(self, team_name):
        """Returns a boolean determining whether a team is in the database."""
        team_exists = False
        self.cur.execute("SELECT school_id FROM school_ids WHERE school_name=%s;", (team_name, ))
        if self.cur.fetchone():
            team_exists = True
        return team_exists
    
    def getPlayerDataForPer(self, player_name):
        """Returns a list of the necessary statistics to calculate one player's PER. This data will 
        be used in another function that calculates PER. We won't need this function if we put PERs in
        the database permanently."""
        try:
            self.cur.execute("SELECT school_id FROM ind_stats WHERE full_name=%s;", (player_name, ))
            sch_id = self.cur.fetchone()
            self.cur.execute("SELECT min, fg, fga, threefg, ft, fta, off_reb, tot_reb, assists, blocks, steals, per_foul, turnovers FROM ind_stats WHERE full_name=%s;", (player_name, ))
            ind_list = self.cur.fetchone()
            self.cur.execute("SELECT tm_points_g, tm_assist, tm_fg, opp_ppg FROM team_stats WHERE school_id=%s;", (sch_id, ))
            team_list = self.cur.fetchone()
            self.cur.execute("SELECT lg_ppg, lg_points, lg_fg, lg_fga, lg_ft, lg_fta, lg_assist, lg_trb, lg_orb, lg_pf, lg_to FROM league_stats;")
            league_list = self.cur.fetchone()
            total_list = []
            for item in ind_list:
                total_list.append(item)
            for item in team_list:
                total_list.append(item)
            for item in league_list:
                total_list.append(item)
        except:
            print 'Problem with getPlayerDataForPer() select query. =('
        return total_list

    def PER(self, (minutes, FG, FGA, threeP, FT, FTA, ORB, TRB, AST, BLK, STL, PF, TO, tm_PPG, tm_AST, tm_FG, opp_PPG, lg_PPG, lg_PTS, lg_FG, lg_FGA, lg_FT, lg_FTA, lg_AST, lg_TRB, lg_ORB, lg_PF, lg_TO, )):
        """Calculates the PER of a given player's statistics in a list."""
        try:
            PER = 0
            if minutes != 0:
                factor = (2.0/3.0) - (0.5*(lg_AST/lg_FG))/(2.0*(lg_FG/lg_FT))
                VOP = lg_PTS/(lg_FGA-lg_ORB+lg_TO+0.44*lg_FTA)
                DRBP = (lg_TRB-lg_ORB)/lg_TRB
                u_PER = (1.0/minutes)*(threeP + (2.0/3.0)*AST + (2.0-factor * (tm_AST/tm_FG))*FG + (FT*0.5*(1.0+(1.0-(tm_AST/tm_FG)) + (2.0/3.0)*(tm_AST/tm_FG))) - VOP*TO - VOP*DRBP*(FGA-FG) - VOP*0.44*(0.44+(0.56*DRBP))*(FTA-FT) + VOP*(1.0-DRBP)*(TRB-ORB) + VOP*DRBP*ORB + VOP*STL + VOP*DRBP*BLK - PF*((lg_FT/lg_PF)-0.44*(lg_FTA/lg_PF)*VOP))
                pace = 2.0*lg_PPG/(tm_PPG+opp_PPG)
                a_PER = u_PER*pace
                PER = a_PER*(10.0/0.2229429066)
        except:
            print 'Problem calculating PER. =('
        return PER
        
    def getPlayersNamesAndTeamByLastName(self, letter):
        """Returns a list of all players (and each player's respective team) whose last name begins 
        with the given letter. This list will be displayed when the user searches for a player by 
        last name."""
        try:
            # Ensure that 'letter' is really a single character
            if len(letter) > 1:   
                letter = letter[0]
            elif len(letter) == 0:
                letter = "A"
        
            letter = letter.capitalize()
            self.cur.execute("SELECT full_name, school_id FROM ind_stats WHERE SUBSTRING(last_name from 1 for 1)=%s ORDER BY last_name ASC;", (letter, ))
            names = []
            sch_ids = []
            names_teams_list = []
            # Save name and school id for each player
            for row in self.cur:
                names.append(row[0])
                sch_ids.append(row[1])
            for i in range(len(sch_ids)):
                self.cur.execute("SELECT school_name FROM school_ids WHERE school_id=%s;", (sch_ids[i], ))
                team = ""
                for row in self.cur:
                    team += row[0]
                names_teams_list.append([names[i], team])
        except:
            print 'Problem with getPlayersNamesAndTeamByLastName() select query. =('
        return names_teams_list
        
    def getTeamNames(self):
        """Returns a list of all of the teams in the conference. This list will be displayed for the
        user to select a team."""
        try:
            self.cur.execute("SELECT school_name FROM school_ids;")
            team_names = []
            for row in self.cur:
                team_names.append(row[0])
        except:
            print 'Problem with getTeamNames() select query. =('
        return team_names
        
    def getPlayersNames(self, team_name):
        """Returns a list of all of the players on a given team ordered by PER. 
        This list will be displayed for the user to find an individual player on a certain team."""
        names = []
        try:
            self.cur.execute("SELECT school_id FROM school_ids WHERE school_name=%s;", (team_name, ))
            sch_id = self.cur.fetchone()
            sch_id = sch_id[0]
            self.cur.execute("SELECT full_name FROM ind_stats WHERE school_id=%s ORDER BY per DESC;", (sch_id, ))
            for name in self.cur:
                names.append(name[0])
        except:
            print 'Problem with getPlayersNames() select query. =('
        return names

    def getLeagueLeaders(self, num_leaders):
        """Returns a list of names of the specified number of league leaders in PER. The list will
        be sorted by descending PER. This data will be used to display the statistics of all league
        leaders in a table."""
        try:
            if num_leaders == "all":
                self.cur.execute("SELECT full_name FROM ind_stats ORDER BY per DESC;")
            else:
                self.cur.execute("SELECT full_name FROM ind_stats WHERE min_avg>=10 ORDER BY per DESC LIMIT %s;", (num_leaders, ))
            league_leaders = []
            for row in self.cur:
                league_leaders.append(row[0])
        except:
            print 'Problem with getLeagueLeaders() select query. =('
        return league_leaders

    def getTeamAndPer(self, player_name):
        """Returns a 3-item list consisting of player name, team name, and PER. This data will be used
        to make the home page table."""
        try:
            self.cur.execute("SELECT school_id,per FROM ind_stats WHERE full_name=%s;", (player_name, ))
            id_and_per = self.cur.fetchone()
            sch_id = id_and_per[0]
            per = id_and_per[1]
            self.cur.execute("SELECT school_name FROM school_ids WHERE school_id=%s;", (sch_id, ))
            team = self.cur.fetchone()
            team = team[0]
            player_info = [player_name, team, per]
        except:
            print 'Problem with getTeamAndPer() select query. =('
        return player_info

def main():
    print 'Content-type: text/html\r\n\r\n'
    print '<html><head></head><body>'
    print 'Running main<br/>'
    ds = DataSource()
    ds.cur.execute("SELECT full_name FROM ind_stats ORDER BY last_name ASC;")
    names =  ds.cur.fetchall()
    for name in names:
        name = name[0]
        print ds.PER(ds.getPlayerDataForPer(name)),"<br/>"
    print 'Finished<br/>'

if __name__ == '__main__':
    main()