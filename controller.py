#Controller.py
#Tristan Leigh and Julia Kroll

import cgi
import cgitb
cgitb.enable()

def getCgiParameters():
    """Returns sanitized, default-populated values for the CGI parameters."""
    form = cgi.FieldStorage()
    parameters = {'stats_type':'', 'team_name':'', 'player_name':'', 'sorting_num':'', 'letter_index':''}
    
    for param in parameters.keys():
        if param in form:
            parameters[param] = sanitizeUserInput(form[param].value)
    
    return parameters

def sanitizeUserInput(s):
    """Strips out scary characters from s and returns the sanitized version.
    """
    chars_to_remove = ";,\\/:\"<>@()"
    for ch in chars_to_remove:
        s = s.replace(ch, '')
    return s