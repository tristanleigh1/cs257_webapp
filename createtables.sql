DROP TABLE IF EXISTS ind_stats;
DROP TABLE IF EXISTS team_stats;
DROP TABLE IF EXISTS league_stats;
DROP TABLE IF EXISTS school_ids;

CREATE TABLE league_stats (
  lg_ppg float,
  lg_points float,
  lg_fg float,
  lg_fga float,
  lg_ft float,
  lg_fta float,
  lg_assist float,
  lg_trb float,
  lg_orb float,
  lg_pf float,
  lg_to float
);
CREATE TABLE ind_stats (
  full_name text,
  first_name text,
  last_name text,
  num int,
  school_id int,
  gp int,
  gs int,
  min int,
  min_avg float,
  fg int,
  fga int,
  fg_fga text,
  fg_pct float,
  threefg int,
  threefga int,
  threefg_threefga text,
  threefg_pct float,
  ft int,
  fta int,
  ft_fta text,
  ft_pct float,
  off_reb int,
  off_reb_g float,
  def_reb int,
  def_reb_g float,
  tot_reb int,
  reb_avg float,
  per_foul int,
  dq int,
  assists int,
  assist_g float,
  turnovers int,
  to_g float,
  a_to float,
  blocks int,
  blk_g float,
  steals int,
  stl_g float,
  points int,
  points_g float,
  per float
);
CREATE TABLE team_stats (
	school_id int,
	tm_points_g float,
	tm_assist int,
	tm_fg int,
	tm_fga int,
	opp_ppg float,
	tm_points int,
	tm_ft int,
	tm_fta int,
	tm_trb int,
	tm_orb int,
	tm_pf int,
	tm_to int
);

CREATE TABLE school_ids (
	school_id int,
	school_name text
);

\copy ind_stats FROM 'ind_stats.csv' DELIMITER ',' CSV
\copy team_stats FROM 'team_stats.csv' DELIMITER ',' CSV
\copy league_stats FROM 'league_stats.csv' DELIMITER ',' CSV
\copy school_ids FROM 'school_ids.csv' DELIMITER ',' CSV