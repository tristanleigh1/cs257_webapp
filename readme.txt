PER-Pal

By Tristan Leigh and Julia Kroll
(leight, krollj)

URL: thacker.mathcs.carleton.edu/cs257/leight/dev/index.py

Description: PER-Pal allows users to view men's basketball statistics for 
players in the Minnesota Intercollegiate Athletic Conference (MIAC).

Features:
-- View one player's statistics, finding the player by either his team or his last name
-- View the statistics of every player on one team
-- View the statistics for the top 5, 10, 25, 50, or 100 league leaders, 
   or all players, sorted by PER
