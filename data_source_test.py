#!/usr/bin/env python
import unittest
from data_source import DataSource
ds = DataSource()


class DataSourceTest(unittest.TestCase):
	"""Test class for data_source.py"""

	def setUp(self):
		""" Setting up for the test """
		pass
	def test_get_player_data_for_table_should_return_correct_data(self):
		expected = ds.getPlayerDataForTable('Shane McSparron')
		actual = [15, 'Shane McSparron', 21, 20, 33.600000000000001, '104-234', 44.399999999999999, '1-5', 20.0, '62-87', 71.299999999999997, 0.33329999999999999, 1.7142857140000001, 2.0, 1.2, 1.8999999999999999, 0.59999999999999998, 0.69999999999999996, 0.69999999999999996, 12.9, 11.629704419999999]
		self.assertEqual(actual, expected)

	def test_is_player_should_return_correct_boolean(self):
		expected = ds.isPlayer('Shane McSparron')
		actual = True
		self.assertEqual(actual, expected)

	def test_is_team_should_return_correct_boolean(self):
		expected = ds.isTeam('Carleton')
		self.assertTrue(expected)

	def test_get_player_data_for_per_should_return_correct_data(self):
		expected = ds.getPlayerDataForPer('Shane McSparron')
		actual = [706, 104, 234, 1, 62, 87, 7, 43, 25, 15, 14, 31, 40, 64.599999999999994, 325, 635, 62.399999999999999, 66.799999999999997, 19382.0, 7230.0, 15727.0, 3278.0, 4691.0, 3802.0, 9471.0, 2857.0, 4364.0, 3304.0]
		self.assertEqual(actual, expected)

	def test_per_should_return_correct_per(self):
		expected = ds.PER([706, 104, 234, 1, 62, 87, 7, 43, 25, 15, 14, 31, 40, 64.599999999999994, 325, 635, 62.399999999999999, 66.799999999999997, 19382.0, 7230.0, 15727.0, 3278.0, 4691.0, 3802.0, 9471.0, 2857.0, 4364.0, 3304.0])
		actual = 11.629704420649665
		self.assertEqual(actual, expected)

	def test_get_players_names_and_team_by_last_name_should_return_correct_names_and_team(self):
		expected = ds.getPlayersNamesAndTeamByLastName('A')
		actual = [['Corey Abbas', 'Concordia'], ['Riley Aeikens', 'St. Olaf'], ['Dylan Alderman', 'Concordia'], ['Marcus Alipate', 'St. Thomas'], ['Jimmy Ancius', 'St. Thomas'], ['Isaac Anderson', 'Concordia']]
		self.assertEqual(actual, expected)

	def test_get_team_names_should_return_correct_teams(self):
		expected = ds.getTeamNames()
		actual = ['Augsburg', 'Bethel', 'Carleton', 'Concordia', 'Gustavus', 'Hamline', 'Macalester', 'Saint John\'s', 'Saint Mary\'s', 'St. Olaf', 'St. Thomas']
		self.assertEqual(actual, expected)

	def test_get_players_names_should_return_correct_players(self):
		expected = ds.getPlayersNames("Carleton")
		actual = ['Kevin Grow', 'Tianen Chen', 'Charles Linneman', 'Beau Smit', 'Shane McSparron', 'John Eckert', 'Mitchell Biewen', 'Ryan Casperson', 'Peter Bakker-Arkema', 'Walker Froehling', 'Malekai Mischke', 'Patrick England']
		self.assertEqual(actual, expected)

	def test_get_league_leaders_should_return_correct_leaders(self):
		expected = ds.getLeagueLeaders(5)
		actual = ['Kyle Zimmermann', 'Ben Figini', 'Jordan Bolger', 'Gary Cooper', 'Kevin Grow']
		self.assertEqual(actual, expected)

	def test_get_team_and_per_should_return_correct_list_of_data(self):
		expected = ds.getTeamAndPer('Shane McSparron')
		actual = ['Shane McSparron', 'Carleton', 11.629704419999999]
		self.assertEqual(actual, expected)

if __name__ == '__main__':
    unittest.main()